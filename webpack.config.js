var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports={
    entry: './src/index.js',
    output:{
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', '.scss']
    },
    module:{
        loaders:[
            {test:/\.(scss)$/, loader:ExtractTextPlugin.extract(['css','sass'])},
            {test:/\.html$/, loader:'html'},
            {test:/\.png$/, loader:'file'},
            {test:/\.js$/, exclude: /node_modules/, loader:'babel-loader?presets[]=es2015'}
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: 'src/index.html'
        }),
        new HtmlWebpackPlugin({
            filename: 'template/track.html',
            template: 'src/template/track.html',
            inject: false
        }),
        new HtmlWebpackPlugin({
            filename: 'template/playlist.html',
            template: 'src/template/playlist.html',
            inject: false
        }),
        new ExtractTextPlugin('style.css')
    ]
};