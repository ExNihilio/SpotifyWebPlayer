Spotify Web Player
=========

This player is just a proof of concept.


## Details

You can see a working demo at http://y.monolizard.com/SpotifyWebPlayer

Player uses AngularJS 1.5 as a framework. Spotify connection is unauthorized, so no login will be needed.
Web speech recognition api used for voice commands which only Chrome supports right now.
Playlists uses offline web storage api which means they are local to your computer.

## Instructions

- Install node.js and npm, if you don't have them https://nodejs.org/en/
- Clone repository.
- Open up your choice of cli.
- Install webpack via
```
npm i -g webpack webpack-dev-server
```
- Install dependencies via
```
npm i
```
- If you want to see the changes you do real time, also install devDependencies via
```
npm i --only=dev
```
- To build use
```
npm run build
```
- To enable dev server which you can access at http://localhost:8080/webpack-dev-server/index.html
```
webpack-dev-server
```

## Searching for a track

Use search input or if you are using Chrome and a microphone use voice command 'search {{song name}}'

## Playlists

Use playlist input to create a playlist. You can remove them by clicking the x button.
By clicking the name of the playlist or using voice command 'open {{playlist name}}', you can see and listen tracks you saved.

## Tracks

Click on the track play button or use voice command 'play {{song name or a part of it}}' to start listening.
Click on addtoplaylist button to open a menu containing current playlists, which you can click to save that track.
You can stop the track by changing context, using track stop button or using voice command 'stop'

## Voice Commands

Be aware voice commands are only active in Chrome. For security reasons which Chrome enforces, before playing a track with voice
you must click a track and play to enable user input.

- search {{song name}} -> Searches for tracks
- play {{song name or a part of it}} -> Plays a track which matches voice input and track names or a part of it
- open {{playlist name}} -> Opens playlist which matches voice input and playlist names
- stop -> Stops current track
