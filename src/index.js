import Angular from 'angular';
import './style.scss';

import audioService from './service/audio.service';
import voiceService from './service/voice.service';
import spotifyService from './service/spotify.service';
import storageService from './service/storage.service';

import trackDirective from './directive/track.directive';
import playlistDirective from './directive/playlist.directive';

import searchController from './controller/search.controller';
import playlistController from './controller/playlist.controller';
import songlistController from './controller/songlist.controller';

const SWP = Angular.module('SWP', []);

SWP.service('audioService', audioService);
SWP.service('voiceService', ['$rootScope', '$window', voiceService]);
SWP.service('spotifyService', ['$http', spotifyService]);
SWP.service('storageService', ['$rootScope', '$window', storageService]);

SWP.directive('trackItem', trackDirective);
SWP.directive('playlist', playlistDirective);

SWP.controller('searchController', ['$scope', '$rootScope', 'spotifyService', 'audioService', searchController]);
SWP.controller('playlistController', ['$scope', '$rootScope', '$timeout', 'storageService', 'spotifyService', 'voiceService', 'audioService', playlistController]);
SWP.controller('songListController', ['$scope', '$compile', 'spotifyService', 'storageService', 'audioService', songlistController]);