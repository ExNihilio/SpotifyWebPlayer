export default function($http){
	var currentResult = {};
	var error = "";

	this.getError = () => { return error; }

	this.getSearchData = () => { return currentResult; }

	this.searchFor = (term) => {
		$http({
		  	method:'GET',
		  	url:'https://api.spotify.com/v1/search?type=track&limit=20&q=' + encodeURIComponent(term)
		}).then(function successCallback(response) {
		   	if(response.statusText == 'OK'){
		   		currentResult = response.data.tracks.items;
		   		if(currentResult.length == 0)
		   			error = 'Nothing found.';
		   	}else{
		   		error = 'Nothing found.';
		   	}
		},function errorCallback(response) {
		    error = 'This call responded with an error. Please try again later.';
		});
	}

	this.getPlaylistData = (trackIds) => {
		var trackQuery = '';
		for (var i = 0; i < trackIds.length; i++) {
			trackQuery += trackIds[i];
			if(i != trackIds.length - 1)
				trackQuery += ",";
		}

		$http({
		  	method:'GET',
		  	url:'https://api.spotify.com/v1/tracks/?ids=' + encodeURIComponent(trackQuery)
		}).then(function successCallback(response) {
		   	if(response.statusText == 'OK'){
		   		currentResult = response.data.tracks;
		   		if(currentResult.length == 0)
		   			error = 'Nothing found.';
		   	}else{
		   		error = 'Nothing found.';
		   	}
		},function errorCallback(response) {
		    error = 'This call responded with an error. Please try again later.';
		});
	}
};