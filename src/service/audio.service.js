export default function(){
	var audio = null;

	this.play = (url) => {
		if(audio == null){
			audio = new Audio(url);
			audio.loop = true;
		}
		else
			audio.src = url;

		audio.play()
	}

	this.stop = () => {
		if(audio != null)
			audio.pause();
	}
};