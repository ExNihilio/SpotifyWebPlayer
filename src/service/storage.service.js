export default function($rootScope, $window){
	var storage = $window.localStorage;
	var loadedPlaylist = '';

	if(storage['playlist'] == null)
		storage['playlist'] = JSON.stringify([]);

	this.createPlaylist = (name) => {
		name = name.toLowerCase();

		var data = JSON.parse(storage['playlist']);
		if(data.filter(v => v.name == name).length > 0 || data.length == 5)
			return;
		data.push({name, tracks:[]});
		storage['playlist'] = JSON.stringify(data);
	}

	this.deletePlaylist = (playlistName) => {
		var data = JSON.parse(storage['playlist']);
		data.splice(data.findIndex(v => v.name == playlistName), 1);
		storage['playlist'] = JSON.stringify(data);
	}

	this.getPlaylists = () => {
		return JSON.parse(storage['playlist']);
	}

	this.getPlaylistTracks = (playlistName) => {
		loadedPlaylist = playlistName;
		return JSON.parse(storage['playlist']).filter(v => v.name == playlistName)[0].tracks;
	}

	this.addTrackToPlaylist = (playlistName, trackId) => {
		var data = JSON.parse(storage['playlist']);
		var playlist = data.filter(v => v.name == playlistName);

		if(playlist[0].tracks.indexOf(trackId) == -1)
			playlist[0].tracks.push(trackId);

		storage['playlist'] = JSON.stringify(data);
	}

	this.removeTrackFromPlaylist = (trackId) => {
		var data = JSON.parse(storage['playlist']);
		var playlist = data.filter(v => v.name == loadedPlaylist);
		var trackIndex = playlist[0].tracks.indexOf(trackId);

		playlist[0].tracks.splice(trackIndex, 1);
		storage['playlist'] = JSON.stringify(data);

		$rootScope.$broadcast('onRemoveTrack', trackIndex);
	}
};