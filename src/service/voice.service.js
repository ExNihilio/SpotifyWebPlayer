export default function($rootScope, $window){
	var recognition = null;
	var isExtracting = false;

	this.init = () => {
		if (!('webkitSpeechRecognition' in $window)) {
			$rootScope.$broadcast('onResultChange', 'This browser doesn\'t support Web Speech Api.');
			$rootScope.$broadcast('onActivityChange', true);
		  	return;
		}else{
			recognition = new webkitSpeechRecognition();

			recognition.onerror = function(event) {
				$rootScope.$broadcast('onResultChange', 'Error: ' + event.error, true);
		 	}

		 	recognition.onend = function(event){
		 		$rootScope.$broadcast('onActivityChange', false);
		 	}

		 	recognition.onresult = function(event) {
		 		if(isExtracting) return;
		 		else isExtracting = true;

		 		var results = [];

		 		for (var i = 0; i < event.results.length; i++) {
			      	if (event.results[i].isFinal) {
				        if(results.indexOf(event.results[i][0].transcript) == -1) results.push(event.results[i][0].transcript);
				    }
			    }

			    $rootScope.$broadcast('onResultChange', results.join(' '), true);

			    isExtracting = false;
			}
		}
	}

	this.record = () => {
		$rootScope.$broadcast('onActivityChange', true);
		$rootScope.$broadcast('onResultChange', 'Waiting for input...');
		recognition.start();
	}
};