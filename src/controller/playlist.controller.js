export default function($scope, $rootScope, $timeout, storageService, spotifyService, voiceService, audioService){
	$scope.playlistData = storageService.getPlaylists();
	$scope.createdPlaylistName = '';
	$scope.speechResult = '';
	$scope.showPsForm = false;

    $scope.$on('onResultChange', function (event, result, doApply) {
	    $scope.speechResult = result;
	    if(doApply) $scope.$apply();
	    result = result.toLowerCase();

	    if(result.match('search')){
	    	audioService.stop();
	    	result = result.replace(/(search )/g, '');
	    	spotifyService.searchFor(result);
	    	$rootScope.$broadcast('onPlaylistShown', false);
	    }

	    if(result.match('play')){
	    	result = result.replace(/(play )/g, '');
	    	var tracks = document.querySelectorAll('.track-name');

			for(var i = 0, l = tracks.length; i < l; i++){
			 	if(tracks[i].innerHTML.toLowerCase().match(result) != null){
			 		angular.element(tracks[i].parentNode.parentNode.querySelector('.play')).triggerHandler('click');
			 	}
			}
	    }

	    if(result.match('stop')){
	    	audioService.stop();
	    }

	    if(result.match('open')){
	    	result = result.replace(/(open )/g, '');

	    	for(var i=0; i < $scope.playlistData.length; i++){
	    		if($scope.playlistData[i].name.toLowerCase() == result){
	    			$scope.loadPlaylistData(result);
	    		}
	    	}
	    }
	});

	$scope.$on('onActivityChange', function (event, result) {
		if(result)
	    	document.getElementById('record').setAttribute('disabled', result);
	    else
	    	document.getElementById('record').removeAttribute('disabled');
	});

	$scope.onFormStateChange = () => {
		$scope.showPsForm = !$scope.showPsForm;
		if($scope.showPsForm){
			$timeout(function(){
				document.getElementById('psInput').focus();
			}, 100);
		}
	}

	$scope.startVoice = () => {
		voiceService.record();
	}

	$scope.onCreatePlaylist = () => {
		if($scope.createdPlaylistName != ''){
			storageService.createPlaylist($scope.createdPlaylistName);
			$scope.playlistData = storageService.getPlaylists();
			$scope.createdPlaylistName = '';
			$rootScope.$broadcast('onCreatePlaylist');
		}
		
		$scope.onFormStateChange();
	}

	$scope.onRemovePlaylist = (item) => {
		storageService.deletePlaylist(item);
		$scope.playlistData = storageService.getPlaylists();
		$rootScope.$broadcast('onRemovePlaylist');
	}

	$scope.loadPlaylistData = (name) => {
		let tracks = storageService.getPlaylistTracks(name);
		if(tracks.length > 0){
			spotifyService.getPlaylistData(tracks);
			$rootScope.$broadcast('onPlaylistShown', true);
			audioService.stop();
		}
	}

	voiceService.init();
};