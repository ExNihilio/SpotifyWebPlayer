export default function($scope, $compile, spotifyService, storageService, audioService){
	$scope.searchData = [];
	$scope.errorInCall = '';
	$scope.isPlaylistShown = false;
	$scope.currentPlayingTrack = null;
	$scope.currentTrackId = 0;
	$scope.currentPlaylist = null;
	$scope.playlistNames = storageService.getPlaylists();

	$scope.$watch(
		() => { return spotifyService.getError(); },
		() => { $scope.errorInCall = spotifyService.getError(); }
	);

    $scope.$watch(
    	() => { return spotifyService.getSearchData(); },
    	() => { $scope.searchData = spotifyService.getSearchData();}
    );

    $scope.$on('onPlaylistShown', function (event, status) { 
	    $scope.isPlaylistShown = status;
	});

	$scope.$on('onRemoveTrack', function (event, index) { 
	    $scope.searchData.splice(index, 1);
	});

	$scope.$on('onCreatePlaylist', function (event) { 
	    $scope.playlistNames = storageService.getPlaylists();
	});

	$scope.$on('onRemovePlaylist', function (event) { 
	    $scope.playlistNames = storageService.getPlaylists();
	    if($scope.playlistNames.length == 0)
	    	$scope.onClosePlaylist();
	});

	$scope.onTrackClick = (event, url) => {
		event.preventDefault();

		$scope.onClosePlaylist();

		if($scope.currentPlayingTrack != null){
			$scope.currentPlayingTrack.querySelector('.play').classList.remove('hidden');
			$scope.currentPlayingTrack.querySelector('.pause').classList.add('hidden');
			$scope.currentPlayingTrack.classList.remove('show-block');

			if($scope.currentPlayingTrack == event.target.parentNode){
				audioService.stop();
				$scope.currentPlayingTrack = null;
				return;
			}
		}

		audioService.play(url);
		$scope.currentPlayingTrack = event.target.parentNode;
		$scope.currentPlayingTrack.querySelector('.play').classList.add('hidden');
		$scope.currentPlayingTrack.querySelector('.pause').classList.remove('hidden');
		$scope.currentPlayingTrack.classList.add('show-block');
	}

	$scope.onAddTrack = (event, id) => {
		angular.element($scope.currentPlaylist).remove();
		$scope.currentTrackId = id;
		$scope.currentPlaylist = $compile("<playlist></playlist>")($scope);
		angular.element(event.target.parentNode.parentNode).append($scope.currentPlaylist);
	}

	$scope.onAddTrackTo = (event, name) => {
		event.preventDefault();

		storageService.addTrackToPlaylist(name, $scope.currentTrackId);
		angular.element(event.target.parentNode.parentNode.parentNode).remove();
	}

	$scope.onRemoveTrack = (id) => {
		storageService.removeTrackFromPlaylist(id);
	}

	$scope.onClosePlaylist = () => {
		angular.element($scope.currentPlaylist).remove();
		$scope.currentPlaylist = null;
	}
};