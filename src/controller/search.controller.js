export default function($scope, $rootScope, spotifyService, audioService){
	$scope.searchTerm = '';
	spotifyService.searchFor('test');

	$scope.onSubmit = () => {
		spotifyService.searchFor($scope.searchTerm);
		$scope.searchTerm = '';
		$rootScope.$broadcast('onPlaylistShown', false);
		audioService.stop();
	};
};